#!/bin/bash



PWD_PATH=`pwd`

### set Lib
LIB_PATH=""

for file in `ls  ${PWD_PATH}"/libs"`
 do
    LIB_PATH=${LIB_PATH}:${PWD_PATH}"/libs/"${file}":"
done


export CLASSPATH=$CLASSPATH:${PWD_PATH}/build/classes/java/main:${PWD_PATH}/build/resources/:${PWD_PATH}/out/production/classes:${LIB_PATH}
echo $CLASSPATH
#### Run TestCase

gradle build
echo "---init production---"
java -jar ${PWD_PATH}/bin/production-init.jar ${PWD_PATH}/src/main/resource/ -cp $CLASSPATH


