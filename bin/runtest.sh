#!/bin/bash                                                                                                                                                                                                                                                                                                                                                        #!/bin/bash

CONFIG_FILE=${1:?"config file is required"}
###### Set JAVA_OPTS

JAVA_CMD=`which java`
JAVA_OPTS="
-Xms1024M
-Xmx3000M
-XX:PermSize=256M
-XX:MaxPermSize=256M
"

if [[ -n "$JMX_HOST" ]] && [[ -n "$JMX_PORT" ]];then
    JAVA_OPTS="
-Dcom.sun.management.jmxremote.port=$JMX_PORT
-Dcom.sun.management.jmxremote.rmi.port=$JMX_PORT
-Dcom.sun.management.jmxremote.authenticate=false
-Dcom.sun.management.jmxremote.ssl=false
-Djava.rmi.server.hostname=$JMX_HOST
$JAVA_OPTS
"
fi

if [ $? -ne 0 ]; then
   echo "Please set java to env path"
   exit
fi

PWD_PATH=`pwd`

### set Lib
LIB_PATH=""

for file in `ls  ${PWD_PATH}"/libs"`
 do
    LIB_PATH=${LIB_PATH}:${PWD_PATH}"/libs/"${file}":"
done

#### set testNG
export TESTNG_HOME=${PWD_PATH}/bin/testNG
echo "TESTNG_HOME="${TESTNG_HOME}

export CLASSPATH=$CLASSPATH:$TESTNG_HOME/testng-7.2.0.jar:$TESTNG_HOME/jcommander-1.29.jar:${PWD_PATH}/build/classes/java/main:${PWD_PATH}/build/resources/:${PWD_PATH}/out/production/classes:${LIB_PATH}


echo $CLASSPATH
#### Run TestCase
rm -rf ../public/*
rm -rf ${TESTNG_HOME}/report
rm -rf build
rm -rf report
rm -rf ${PWD_PATH}/src/main/resource/auto*

mkdir -p ../public

gradle build
echo "---init production---"
java -jar ${PWD_PATH}/bin/production-init.jar ${PWD_PATH}/src/main/resource ${CONFIG_FILE} -cp $CLASSPATH
echo "---start run case---"
java -Dtestng.dtd.http=true org.testng.TestNG -parallel methods -threadcount 10 ${PWD_PATH}/src/main/resource/testng.xml  -d ${CONFIG_FILE%.*}

#TestNG by default disables loading DTD from unsecured Urls. If you need to explicitly load the DTD from a http url, please do so by using the JVM argument [-Dtestng.dtd.http=true]