package com.company.test.base;

import java.util.*;

public class CommonUtils {


    public static String insertStrToString(String list,String insert){
        if(list.equals("[]")){
            return "[\""+insert+"\"]";
        }
        String tempStr = list.substring(0,list.length()-2);
        return tempStr +",\""+ insert + "\"]";
    }

    /**
     *
     * for example :
     *  list  : "member"
     *  insert : ["A","B"]
     *  result: ["A","B","member"]
     *
     * */

    public static String insertListToString(String list,List<String> insert){
        String templist=list;
        for(String str:insert){
            templist = insertStrToString(templist,str);
        }
        return templist;
    }
}