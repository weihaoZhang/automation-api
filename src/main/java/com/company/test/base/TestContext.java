package com.company.test.base;

import java.util.List;

public interface TestContext {
    public static final String PREFIX = "company.";

    public Object getAttribute(String key);

    public List<Object> getAttributes(String key);

    public void setAttribute(String key, Object attr);

    public boolean containsAttribute(String key);

    public Object removeAttribute(String key);
}