package com.company.test.base;


public class CompanyCase {
    private String _testCaseId = null;
    private TestContext _context;

    protected CompanyCase(final String id) {
        _testCaseId = id;
        _context = ContextRegistry.getContext();
    }

    protected CompanyCase() {
        _testCaseId = getClass().getSimpleName();
        _context = ContextRegistry.getContext();
    }

    /**
     * Get case id
     * @return case id
     */
    public final String getId() {
        return _testCaseId;
    }

    public final TestContext getContext(){
        return _context;
    }

    public final int getWorkspaceId(){
        return Integer.parseInt(PropertiesHelper.getProperty().get("workspaceId"));
    }

    public final int getOrganisationId(){
        return Integer.parseInt(PropertiesHelper.getProperty().get("organisationId"));
    }
}
