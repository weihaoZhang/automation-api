package com.company.test.base;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class BaseRequest {


    private static final Logger logger = LoggerFactory.getLogger(com.company.test.base.BaseRequest.class);


    public static  String postResource(String url, String jsonString){
        Map<String,String> header = new HashMap<>();
        return postResource(url,jsonString,header,PropertiesHelper.getProperty().get("token.my"));
    }

    public static String postResource(String url, String jsonString, Map<String,String> header){
        return postResource(url,jsonString,header,PropertiesHelper.getProperty().get("token.my"));
    }

    public static String  postResource(String url, String jsonString, Map<String,String> header,String token){
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(30000).setConnectionRequestTimeout(30000).setSocketTimeout(30000).build();
        HttpPost httpPost = new HttpPost(getPath(url));
        httpPost.setConfig(requestConfig);
        httpPost.setConfig(requestConfig);
        httpPost.addHeader("Content-Type", "application/json;charset=UTF-8");
        httpPost.setHeader("Accept", "application/json");
        httpPost.addHeader("authorization",token);
        httpPost.setEntity(new StringEntity(jsonString, "UTF-8"));

        Set<String> keys =  header.keySet();
        for(String key : keys){
            if( httpPost.getHeaders(key).length > 0){
                httpPost.removeHeaders(key);
            }
            httpPost.addHeader(key,header.get(key));
        }

        logger.info("url:" + url + "\nbody:" + jsonString+ "---------------");
        return HCUntil.doPost(httpPost);
    }

    public static String deleteMethoResource(String url){
        return deleteMethoResource(url,"");
    }

    public static String deleteMethoResource(String url,String token){
        if(token.equalsIgnoreCase("")){
            token = PropertiesHelper.getProperty().get("token.my");
        }
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(30000).setConnectionRequestTimeout(30000).setSocketTimeout(30000).build();
        HttpDelete httpDelete = new HttpDelete(getPath(url));
        httpDelete.setConfig(requestConfig);
        httpDelete.setConfig(requestConfig);
        httpDelete.setHeader("Accept", "application/json");
        httpDelete.addHeader("authorization",token);
        return HCUntil.doDelete(httpDelete);
    }



    public static String getResource(String url){
        HttpGet httpGet = new HttpGet(getPath(url));
        httpGet.addHeader("authorization",PropertiesHelper.getProperty().get("token.my"));
        return HCUntil.doGet(httpGet);
    }

    public static  String putResource(String url, String jsonString){
       return putResource(url,jsonString,PropertiesHelper.getProperty().get("token.my"));
    }

    public static String putResource(String url, String jsonString,String token){
        if(token.equalsIgnoreCase("")){
            return postResource(url,jsonString);
        }
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(30000).setConnectionRequestTimeout(30000).setSocketTimeout(30000).build();
        HttpPut httpPut = new HttpPut(getPath(url));
        httpPut.setConfig(requestConfig);
        httpPut.setConfig(requestConfig);
        httpPut.addHeader("Content-Type", "application/json;charset=UTF-8");
        httpPut.setHeader("Accept", "*/*");
        httpPut.addHeader("authorization",token);
        httpPut.setEntity(new StringEntity(jsonString, "UTF-8"));

        logger.info("\n-----------------url:" + url + "\nbody:" + jsonString+ "---------------");
        Reporter.log("\n-----------------url:" + url + "body:"+jsonString);

        return HCUntil.doPut(httpPut);
    }


    private static String getPath(String url){
        return "https://" + url;
    }

    public static  String getResource(String url, String jsonString){

        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(30000).setConnectionRequestTimeout(30000).setSocketTimeout(30000).build();
        HttpGet httpGet = new HttpGet(getPath(url));
        httpGet.setConfig(requestConfig);
        httpGet.setConfig(requestConfig);
        httpGet.addHeader("Content-Type", "application/json;charset=UTF-8");
        httpGet.setHeader("Accept", "application/json");
        httpGet.addHeader("authorization",PropertiesHelper.getProperty().get("token.one"));
        Reporter.log("\n-----------------url:" + url + "body:"+jsonString);
        //logger.info("\n-----------------url:" + url + "\nbody:" + jsonString+ "---------------");
        return HCUntil.doGet(httpGet);
    }
}
