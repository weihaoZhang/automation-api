package com.company.test.base;

import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

public class TestngListener extends TestListenerAdapter {

    @Override
    public void onTestSuccess(ITestResult tr) {
        super.onTestSuccess(tr);
        System.out.println("success: " + tr.getTestClass().getName());
        Retry retry = (Retry) tr.getMethod().getRetryAnalyzer();
        retry.reset();
    }

    @Override
    public void onTestFailure(ITestResult tr) {
        super.onTestFailure(tr);
        System.out.println("fails: " + tr.getTestClass().getName());

        Retry retry = (Retry) tr.getMethod().getRetryAnalyzer();
        retry.reset();
    }

    @Override
    public void onTestSkipped(ITestResult tr){
        super.onTestSkipped(tr);
        System.out.println("skip:"+ tr.getTestClass().getName());
        tr.setStatus(2);
    }
}
