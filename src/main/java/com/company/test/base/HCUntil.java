package com.company.test.base;


import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.*;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import java.io.IOException;



/**
 * HttpClient Request
 */
public class HCUntil {

    private static final Logger logger = LoggerFactory.getLogger(com.company.test.base.HCUntil.class);
    private static final int SUCCESS_CODE = 200;

    public static String doGet(HttpGet httpGet) {

        String strResult = "";
        CloseableHttpClient client = HttpClients.createDefault();
        try {
            CloseableHttpResponse resp = client.execute(httpGet);
            strResult = EntityUtils.toString(resp.getEntity(),"UTF-8");
            Reporter.log("response:"+strResult);
        } catch (ClientProtocolException e) {
            logger.error("get request false:", e);
            Reporter.log("response:"+e);

            // e.printStackTrace();
        }  catch (IOException e) {
            logger.error("get IO Exception:", e);
            Reporter.log("response:"+e);
            // e.printStackTrace();
        } finally {
            try {
                client.close();
            } catch (Exception e) {
                logger.error("get close exception:", e);
                Reporter.log("response:"+e);
                // e.printStackTrace();
            }
        }
        return strResult;
    }




    /**
     * send POST request By form
     */
    public static String doDelete(HttpDelete httpDelete) {
        CloseableHttpResponse response = null;
        String result = "";

        try {
            CloseableHttpClient httpClient = HttpClients.createDefault();
            response = httpClient.execute(httpDelete);
            result = EntityUtils.toString(response.getEntity(),"UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
            Reporter.log("response:"+e);
        } finally {
            try {
                if (null != response) {
                    response.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
                Reporter.log("response:"+e);
            }
        }
        return result;
    }

    public static String doPost(HttpPost httpPost) {
        CloseableHttpResponse response = null;
        String result = "";

        try {
            CloseableHttpClient httpClient = HttpClients.createDefault();

            response = httpClient.execute(httpPost);
            result = EntityUtils.toString(response.getEntity(),"UTF-8");

            logger.info("response:"+result);
            Reporter.log("response:"+result);
        } catch (Exception e) {
            logger.error("get IO Exception:", e);
            Reporter.log("response:"+e);
        } finally {
            try {
                if (null != response) {
                    response.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
                Reporter.log("response:"+e);
            }
        }
        return result;
    }

    public static String doPut(HttpPut httpPut){
        CloseableHttpResponse response = null;
        String result = "";

        try {
            CloseableHttpClient httpClient = HttpClients.createDefault();
            response = httpClient.execute(httpPut);
            result = EntityUtils.toString(response.getEntity(),"UTF-8");
            logger.info("response:"+result);
            Reporter.log("response:"+result);
        } catch (Exception e) {
            logger.error("get IO Exception:", e);
            Reporter.log("response:"+e);
        } finally {
            try {
                if (null != response) {
                    response.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
                Reporter.log("response:"+e);
            }
        }
        return result;
    }

}