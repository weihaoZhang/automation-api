package com.company.test.base;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class ContextRegistry {
    private static final Map<UUID, TestContext> _contexts = new ConcurrentHashMap<UUID, TestContext>();
    private static final Map<Thread, UUID> _threads = new ConcurrentHashMap<Thread, UUID>();


    public static void setContext(UUID id, TestContext context) {
        _contexts.put(id, context);
    }

    public static void removeContext(UUID id) {
        _contexts.remove(id);
    }

    static void clearThreads(UUID id) {
        for (Map.Entry<Thread, UUID> entry : _threads.entrySet()) {
            if (entry.getValue().equals(id)) {
                _threads.remove(entry.getKey());
            }
        }
    }

    public static void registerThread(Thread t, UUID id) {
        _threads.put(t, id);
    }

    public static void unregisterThread(Thread t) {
        _threads.remove(t);
    }

    public static TestContext getContext() {
        UUID id = _threads.get(Thread.currentThread());
        if (id == null) {
            return null;
        } else {
            return _contexts.get(id);
        }
    }
}