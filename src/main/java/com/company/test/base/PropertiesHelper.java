package com.company.test.base;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class PropertiesHelper {

    private static Map<String, String> _properties = new HashMap<>();

    public static Map<String, String> getProperty(){

        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd");
        String time = sdf.format(date);

        return getCustomProperty("auto"+time+".properties");
    }
    @Deprecated
    public static Map<String, String> getCustomProperty(String filename){
        if(_properties.size() > 0){
            return _properties;
        }
        Properties properties = new Properties();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("src/main/resource/"+filename));
            properties.load(bufferedReader);

            Set<Object> objects = properties.keySet();
            for (Object object : objects) {
                _properties.put(object.toString(),new String(properties.getProperty((String) object).getBytes("iso-8859-1")));
            }
        } catch (
                IOException e) {
            e.printStackTrace();
        }
        return _properties;
    }

    public static boolean exportProperty(String toFile ,Map<String,String> properties){
        try{
            FileOutputStream oFile = new FileOutputStream("src/main/resource/"+toFile, true);
            Properties pps = new Properties();
            Set<String> keys = properties.keySet();

            for(String key : keys){
                pps.put(key,properties.get(key));
            }

            pps.store(oFile, "The New properties file");
            oFile.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return true;
    }

    public static void setProperty(Map<String,String> args){
        Set<String> keys = args.keySet();
        for(String key: keys){
            _properties.put(key,args.get(key));
        }
    };
}
