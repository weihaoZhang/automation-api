package com.company.test.base;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class Retry implements IRetryAnalyzer {

    private int retryCnt = 0;
    private int maxRetryCnt = 2;


    @Override
    public boolean retry(ITestResult iTestResult) {
        if (retryCnt<maxRetryCnt){
            retryCnt++;
            return true;
        }
        return false;
    }


    public void reset() {
        retryCnt = 0;
    }


}
